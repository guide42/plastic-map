(function() {
  const DEBUG = false;
  const KEYDELAY = 1000;  /* ms to start repeating */
  const KEYINTERVAL = 40; /* ms between repetitions */
  const KEYS = ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'Enter'];
  const PROVIDERS = {
    'opentopomap.org': {
      urlTemplate: "https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png",
      attribution: "© OpenStreetMap-Mitwirkende, SRTM | Kartendarstellung: © OpenTopoMap (CC-BY-SA)",
    },
    'openstreetmap.org': {
      urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
      attribution: "© OpenStreetMap contributors",
    },
  };
  const SYMBOLS = [
    'wonder', 'walk', 'view',
    'sleep', 'water', 'market',
    'hitch', 'connect', 'social',
  ];

  /* Leaflet objects */
  let map;          /* the global L.Map */
  let layer;        /* current L.TileLayer, could be L.TileLayer.PlasticMap */
  let bounds;       /* L.LatLngBounds or undefined for all world */
  let feature;      /* current L.FeatureGroup, used for GeoJSON and GPX */
  let marker;       /* new or editing or selected L.Marker */
  let markers = []; /* list of L.Marker or undefined, matches mapmarks */
  let circle;       /* accuracy L.Circle for last know position */

  const mapOptions = {
    attributionControl: false, /* screen space is precious */
    zoomControl: false,
    trackResize: false,
    boxZoom: false,
    doubleClickZoom: false,
    dragging: false,
    minZoom: 0,
    maxZoom: 18,
    keyboard: true,
    keyboardPanDelta: 160,
    scrollWheelZoom: false,
    tap: false,
  };

  const scaleOptions = {
    position: 'bottomleft',
    metric: true,
    imperial: false,
  };

  const popupOptions = {
    closeButton: false,
    offset: [0, -25],
  };

  const pointOptions = {
    radius: 1,
    weight: 1,
    color: '#ff0e0e',
    stroke: false,
    fill: true,
    fillOpacity: 1.0,
    interactive: false,
  };

  let config = {
    markers: [],
    remotes: [],
  };

  let screen = 'welcome';
  let context = {};

  let mode = 'off';    /* on or off */
  let loaded;          /* current provider/map */
  let mapmarks = [];   /* list of {pos,obs,sym} markers */
  let step;            /* delta for moving */
  let maps = [];       /* list of PlasticMap instances */
  let files = [];      /* list of File with GeoJSON and GPX from storage */
  let remotes = [];    /* list of Download instances */
  let geopos = null;   /* last known {lat,lng,alt} geo position */
  let geoextra = null; /* extra GPS information shown on geodata() */

  let storage;
  let keystate = {};

  let PlasticMap = L.Class.extend({
    initialize: function(name, bounds, buffer) {
      this.name = name ? name : ':memory:';
      this.bounds = bounds; /* can be Array[4] or undefined */
      this.buffer = buffer; /* a function that returns a promise */
    },
    getName: function() {
      return this.name.split('/').pop().substring(0, name.lastIndexOf('.'));
    },
    getBounds: function() {
      if (Array.isArray(this.bounds) && this.bounds.length === 4) {
        return L.latLngBounds(
          L.latLng(this.bounds[0], this.bounds[1]),
          L.latLng(this.bounds[2], this.bounds[3])
        );
      }
    },
    arrayBuffer: function() {
      if (typeof this.buffer === 'function') {
        let ret = this.buffer();
        if (ret instanceof Promise) {
          return ret;
        }
      }
      return Promise.reject(new Error(
        `Invalid buffer for '${this.getName()}' plastic map.`
      ));
    },
  });

  function filemap(file, bounds) {
    return new PlasticMap(file.name, bounds, function() {
      if (typeof file.arrayBuffer === 'function') {
        return file.arrayBuffer();
      }
      return new Promise(function(resolve, reject) {
        let reader = new FileReader();

        reader.onerror = function(event) {
          reader.abort();
          reject(event.target.error);
        };
        reader.onloadend = function(event) {
          resolve(event.target.result);
        };

        reader.readAsArrayBuffer(file);
      });
    });
  }

  function remotemap(url, bounds) {
    return new PlasticMap(url, bounds, function() {
      return new Promise(function(resolve, reject) {
        let request = new XMLHttpRequest({mozSystem:true});

        request.onload = function() {
          resolve(request.response);
        };
        request.onerror = function() {
          reject(new Error(`Invalid remote '${url}'. ${request.statusText}.`));
        };

        request.open('GET', url);
        request.responseType = 'arraybuffer';
        request.send();
      });
    });
  }

  function buffermap(buffer, bounds) {
    if (!(buffer instanceof ArrayBuffer)) {
      throw new Error("Invalid buffer.");
    }
    return new PlasticMap(undefined, bounds, function() {
      return Promise.resolve(buffer);
    });
  }

  let Download = L.Class.extend({
    initialize: function(spec) {
      this.url = spec.url;
      this.name = 'name' in spec ? spec.name : spec.url.split('/').pop();
      this.bounds = spec.bounds; /* optional, Array[4] */
      this.size = spec.size; /* optional, can be string or number of bytes */
    },
    isMap: function() {
      return this.url.split('.').pop() === 'mbtiles';
    },
    getUrl: function() {
      return this.url;
    },
    getName: function() {
      return this.name;
    },
    getBounds: function() {
      if (Array.isArray(this.bounds) && this.bounds.length === 4) {
        return L.latLngBounds(
          L.latLng(this.bounds[0], this.bounds[1]),
          L.latLng(this.bounds[2], this.bounds[3])
        );
      }
    },
    getSize: function() {
      return this.size;
    },
    resolve: function() {
      return Promise.resolve([this]);
    },
  });

  Download.create = function(spec) {
    switch (spec.type) {
      case 'remotes':
        return new DownloadRemotes(spec);
    }
    return new Download(spec);
  };

  let DownloadRemotes = Download.extend({
    isRemotes: function() {
      return this.url.split('.').pop() === 'json';
    },
    remotes: [],
    resolve: function() {
      if (this.remotes.length > 0) {
        return Promise.resolve(this.remotes);
      }
      let url = this.getUrl();
      if (!url || typeof url !== 'string' || !this.isRemotes()) {
        return Promise.reject(new Error("Invalid url to download remotes."));
      }
      return new Promise(function(resolve, reject) {
        let request = new XMLHttpRequest({mozSystem:true});

        request.onload = function() {
          resolve(request.response);
        };
        request.onerror = function() {
          reject(new Error(`Invalid remote '${url}'. ${request.statusText}.`));
        };

        request.open('GET', url);
        request.responseType = 'json';
        request.send();
      }).then(data => {
        this.remotes = [];

        if (typeof data === 'array' || data instanceof Array) {
          data.forEach((downspec) => {
            this.remotes.push(Download.create(downspec));
          });
        }

        return this.remotes;
      });
    },
  });

  function init() {
    if (navigator.getDeviceStorages) {
      let sdcard = navigator.getDeviceStorages('sdcard');
      if (sdcard.length > 0) {
        storage = sdcard.pop();
      }
    }

    if (localStorage) {
      let rawgeopos = localStorage.getItem('geopos');
      if (rawgeopos) {
        let geoposidx = rawgeopos.split(',');
        geopos = {
          lat: geoposidx[0],
          lng: geoposidx[1],
          alt: geoposidx[2],
        };
        geoextra = null;
      }
    }

    map = L.map('map', mapOptions);
    map.fitWorld();

    map.on('zoomend', function(event) {
      zoom(0); // invalidateSize
    });
    map.on('locationfound', function(event) {
      geopos = {
        lat: event.latlng.lat,
        lng: event.latlng.lng,
        alt: event.altitude,
      };
      geoextra = {
        accuracy: event.accuracy,
        altitudeAccuracy: event.altitudeAccuracy,
        heading: event.heading,
        speed: event.speed,
      };
      point(event.latlng, event.accuracy);
    });
    map.on('locationerror', function(event) {
      error("Geolocation error.", event);
    });

    L.control.scale(scaleOptions).addTo(map);

    load('maps').then(function(filelist) {
      filelist.forEach(function(file) {
        let ext = file.name.split('.').pop();
        if (ext === 'mbtiles') {
          maps.push(filemap(file));
        } else if (ext === 'geojson' || ext === 'gpx') {
          files.push(file);
        }
      });
      if (maps.length > 0) {
        if (files.length > 0) {
          toast(`${maps.length} map${(maps.length === 1 ? '' : 's')}
            and ${files.length} other file${(files.length === 1 ? '' : 's')}
            available.`);
        } else {
          toast(`${maps.length} map${(maps.length === 1 ? '' : 's')} available.`);
        }
      }
    })
    .catch(function(err) {
      error("Maps cannot be loaded.", err);
    })
    .then(function() {
      return read('plastic-map.json');
    })
    .then(function(data, file) {
      config = L.extend(config, data);

      if (typeof config.center === 'array' || config.center instanceof Array) {
        let center = L.latLng(config.center[0], config.center[1]);
        let zoom = config.center[2];
        if (map) {
          map.flyTo(center, zoom);
        }
      }
      if (typeof config.mode === 'string') {
        mode = config.mode === 'on' ? 'on' : 'off';
      }
      if (typeof config.markers === 'array' || config.markers instanceof Array) {
        config.markers.forEach(function(confmark) {
          mapmarks.push(confmark);
        });
      }
      if (typeof config.remotes === 'array' || config.remotes instanceof Array) {
        if (config.remotes.length > 0) {
          remotes[remotes.length] = config.remotes.map(function(spec) {
            return Download.create(spec);
          });
        }
      }
      if (mode === 'on' && typeof config.provider === 'string') {
        reload(config.provider);
      }
      if (mode === 'off' && typeof config.defaultmap === 'string') {
        reload(config.defaultmap);
      }
      if (mapmarks.length) {
        toast(mapmarks.length === 1
          ? "Configuration and 1 marker loaded."
          : `Configuration and ${mapmarks.length} markers loaded.`
        );
      } else {
        toast("Configuration loaded.");
      }
    })
    .catch(function(err, file) {
      error("Configuration error.", err);
    });

    hide();
    zoom(0); // invalidateSize

    if (geopos) {
      point(L.latLng(geopos));
    }
  }

  function state() {
    let state = L.extend({}, config);

    if (mode === "on") {
      state['provider'] = loaded;
      if (typeof config.defaultmap === 'string') {
        state['defaultmap'] = config.defaultmap;
      }
    } else {
      state['defaultmap'] = loaded;
      if (typeof config.provider === 'string') {
        state['provider'] = config.provider;
      }
    }

    state['mode'] = mode;
    state['center'] = [
      map.getCenter().lat,
      map.getCenter().lng,
      map.getZoom()
    ];

    state['markers'] = mapmarks.filter(function(mark) {
      return typeof mark.deleted === 'undefined' || mark.deleted === false;
    });

    return state;
  }

  function changed() {
    if (mode === 'on') {
      if (config.provider !== loaded) {
        return true;
      }
    } else {
      if (config.defaultmap !== loaded) {
        return true;
      }
    }
    if (config.mode !== mode) {
      return true;
    }
    if (map) {
      let left = map.getCenter();
      let right = L.latLng(config.center[0], config.center[1]);
      if (!left.equals(right, 5)) {
        return true;
      }
    }
    if (config.markers.length !== mapmarks.length) {
      return true;
    }
    if (mapmarks.reduce(
      (c, m) => typeof m.deleted !== 'undefined' || m.deleted === true, 0) > 0
    ) {
      return true; // with at least one deleted
    }
    return false;
  }

  function read(filename) {
    if (!storage) {
      return Promise.reject(new Error("No device storage found."));
    }

    return new Promise(function(resolve, reject) {
      let request = storage.get(filename);

      request.onerror = function() {
        reject(this.error);
      };
      request.onsuccess = function() {
        let file = this.result;
        let reader = new FileReader();

        reader.onerror = function(event) {
          reader.abort();

          if (event.target.error) {
            reject(event.target.error, file);
          } else {
            reject(undefined, file);
          }
        };
        reader.onloadend = function(event) {
          let data;
          try {
            data = JSON.parse(event.target.result);
          } catch (err) {
            reject(err, file);
            return;
          }

          resolve(data, file);
        };

        reader.readAsText(file);
      };
    });
  }

  function save(filename, data) {
    if (!storage) {
      return Promise.reject(new Error("No device storage found."));
    }

    return new Promise(function(resolve, reject) {
      let request = storage.getEditable(filename);

      request.onerror = function() {
        if (this.error.name === 'NotFoundError') {
          let json = JSON.stringify(data, null, 2);
          let file = new Blob([json], {type: 'application/json'});
          let creating = storage.addNamed(file, filename);

          creating.onerror = function (err) {
            reject(this.error);
          };
          creating.onsuccess = function() {
            resolve(file);
          };
        } else {
          reject(this.error);
        }
      };
      request.onsuccess = function() {
        let handle = this.result;
        if (handle instanceof IDBMutableFile) {
          let lockedFile = handle.open('readwrite');
          lockedFile.location = 0;
          let truncating = lockedFile.truncate();

          truncating.onerror = function() {
            reject(this.error);
          };
          truncating.onsuccess = function() {
            let json = JSON.stringify(data, null, 2);
            let writing = lockedFile.write(json);

            writing.onerror = function() {
              reject(this.error);
            };
            writing.onsuccess = function() {
              let flushing = lockedFile.flush();
              flushing.onerror = function() {
                reject(this.error);
              };
              flushing.onsuccess = function() {
                resolve(lockedFile);
              };
            };
          };
        } else if (handle instanceof File) {
          let deleting = storage.delete(filename);

          deleting.onerror = function () {
            reject(this.error);
          };
          deleting.onsuccess = function() {
            let json = JSON.stringify(data, null, 2);
            let file = new Blob([json], {type: "application/json"});
            let creating = storage.addNamed(file, filename);

            creating.onerror = function () {
              reject(this.error);
            };
            creating.onsuccess = function() {
              resolve(file);
            };
          };
        } else {
          reject(new Error("File cannot be saved."));
        }
      };
    });
  }

  function load(directory) {
    if (!storage) {
      return Promise.reject(new Error("No device storage found."));
    }

    return new Promise(function(resolve, reject) {
      let request = storage.enumerate(directory);
      let filelist = [];

      request.onerror = function() {
        reject(this.error);
      };
      request.onsuccess = function() {
        if (this.result) {
          filelist.push(this.result);

          if (!this.done) {
            this.continue();
          }
        } else {
          resolve(filelist);
        }
      };
    });
  }

  function zoom(delta) {
    if (map && screen === 'map') {
      let zoom = map.getZoom();
      let next = zoom + delta;

      if (layer instanceof L.TileLayer.PlasticMap) {
        if (delta < 0 && next < layer.options.minzoom) {
          toast("Minimum zoom level for current map reached.");
        }
        if (delta > 0 && next > layer.options.maxzoom) {
          toast("Maximum zoom level for current map reached.");
        }
      }

      if (delta > 0) {
        map.zoomIn(delta);
      }
      if (delta < 0) {
        map.zoomOut(delta * -1);
      }

      step = 0.0001;
      if (zoom < 17) { step = 0.001; }
      if (zoom < 16) { step = 0.0025; }
      if (zoom < 15) { step = 0.005; }
      if (zoom < 14) { step = 0.025; }
      if (zoom < 13) { step = 0.075; }
      if (zoom < 12) { step = 0.01; }
      if (zoom < 11) { step = 0.05; }
      if (zoom < 9) { step = 0.1; }
      if (zoom < 7) { step = 0.5; }
      if (zoom < 5) { step = 1; }

      if (delta === 0) {
        map.invalidateSize(false);
      }
    }
  }

  function move(lat, lng) {
    if (map) {
      let center = map.getCenter();

      lat = center.lat + lat * 0.5;
      lng = center.lng + lng;

      if (bounds && !bounds.contains([lat, lng]) && bounds.contains(center)) {
        toast("Moving outside current map bounds.");
      }

      map.panTo([lat, lng]);
    }
  }

  function locate() {
    if (map) {
      map.locate({
        maximumAge: 0, /* don't cache */
        timeout: 30000, /* fail after 30 seconds */
        enableHighAccuracy: true,
        setView: true,
      });
    }
  }

  function point(pos, accuracy) {
    if (map) {
      if (circle) {
        circle.removeFrom(map);
        circle = undefined;
      }
      if (!pos) {
        pos = map.getCenter();
      }
      if (accuracy && accuracy > 1) {
        circle = L.circle(pos, {radius: accuracy}).addTo(map);
      }
      L.circle(pos, pointOptions).addTo(map);
    }
  }

  function mark(pos) {
    if (map) {
      if (!pos) {
        pos = map.getCenter();
      }

      if (marker) {
        marker.setLatLng(pos).update();
        if (typeof marker.markindex === 'number') {
          mapmarks[marker.markindex].pos = pos;
        }
      } else {
        marker = L.marker(pos).addTo(map);
      }

      map.panTo(pos);
    }
  }

  function movemark(delta, all) {
    if (markers.length === 0) {
      toast("No markers yet created.");
      return;
    }

    if (!all && markers.reduce(
      (c, m) => c + (typeof m.markindex === 'undefined' ? 0 : 1), 0) === 0
    ) {
      toast("No markers in bounds.");
      return;
    }

    let current = delta < 0 ? mapmarks.length : -1;
    if (marker && typeof marker.markindex === 'number') {
      current = marker.markindex;
    }

    let markindex;
    for (let r = 0; r < mapmarks.length; r++) {
      let rindex = current + delta;
      if (rindex < 0) {
        rindex = mapmarks.length + current + delta;
      } else if (rindex >= mapmarks.length) {
        rindex = -1 + delta;
      }
      if (typeof markers[rindex] !== 'undefined' &&
        (all || typeof markers[rindex].markindex === 'number')
      ) {
        markindex = rindex;
        break;
      }
      delta += delta < 0 ? -1 : 1;
    }

    if (all) {
      markers[markindex].markindex = markindex;
    }

    if (marker) {
      marker.closePopup();
      if (map && typeof marker.markindex === 'undefined') {
        marker.removeFrom(map);
      }
    }

    marker = markers[markindex];

    if (map) {
      map.flyTo(marker.getLatLng());

      if (all) {
        markers[markindex].addTo(map);
      }
    }

    marker.openPopup();
  }

  function delmark(mark) {
    let markmarker = false;
    if (marker && !mark) {
      mark = marker;
      markmarker = true;
    }
    if (!mark) {
      return;
    }
    if (typeof mark.markindex === 'number') {
      let data = mapmarks[mark.markindex];
      let sym = data.sym || 'wonder';
      let latlng = L.latLng(data.pos);
      bottom({
        "No": function() {
          bottom();
        },
        "Delete": function() {
          mapmarks[mark.markindex].deleted = true;
          mark.closePopup();
          if (map) {
            mark.removeFrom(map);
          }
          if (markmarker) {
            marker = undefined;
          }
          remark();
          bottom();
        },
      }, 'Delete'
        + ` <img src="/sym/${sym}.png" alt="${sym}" width=16 height=16>`
        + ` <code>${OLC.encode(latlng.lat, latlng.lng)}</code>`
        + ` <small>[${latlng.lat.toFixed(4)}, ${latlng.lng.toFixed(4)}]?`
      );
    } else {
      mark.closePopup();
      if (map) {
        mark.removeFrom(map);
      }
      if (markmarker) {
        marker = undefined;
      }
    }
  }

  function remark(mark) {
    if (!map) {
      return;
    }

    markers.forEach(function(oldmarker) {
      oldmarker.removeFrom(map);
    });

    markers = [];

    mapmarks.forEach(function(mapmark, markindex) {
      if (typeof mapmark.deleted !== 'undefined' && mapmark.deleted === true) {
        return; // skip deleted markers
      }

      let latLng = L.latLng(mapmark.pos);
      let orientation;

      if (geopos) {
        let geoLatLng = L.latLng(geopos);
        let distance = latLng.distanceTo(geoLatLng);
        if (distance >= 1000) {
          orientation = (distance / 1000).toFixed(1) + 'km';
        } else {
          orientation = distance.toFixed(0) + 'm';
        }
        orientation += " "
          + (latLng.lat > geoLatLng.lat ? 'N' : 'S')
          + (latLng.lng > geoLatLng.lng ? 'E' : 'W');
      }

      let content = (`
        <code>${OLC.encode(latLng.lat, latLng.lng)}</code>
        <span class="pos">[${latLng.lat.toFixed(4)}, ${latLng.lng.toFixed(4)}]</span>
      `);
      if (orientation) {
        content += `<br><span class="orient">${orientation}</span>`;
      }
      if (mapmark.obs) {
        content += `<p>${mapmark.obs}</p>`;
      }

      markers[markindex] = L.marker(latLng, {
        keyboard: false,
        title: mapmark.obs || '',
        alt: mapmark.sym || 'wonder',
        icon: L.icon.symbol(mapmark.sym || 'wonder'),
      })
      .bindPopup(`<div class="mark">${content}</div>`, popupOptions);

      if ((mark && latLng.equals(mark.pos))
        || !bounds
        || bounds.contains(latLng)
      ) {
        markers[markindex].markindex = markindex;
        markers[markindex].addTo(map);
      }
    });
  }

  function reload(name) {
    if (!map) {
      return;
    }

    if (layer) {
      if (layer instanceof L.TileLayer.PlasticMap) {
        layer.closeDatabase();
      }
      map.removeLayer(layer);
      layer = undefined;
      bounds = undefined;
    }
    if (feature) {
      feature.removeFrom(map);
      feature = undefined;
    }

    if (typeof name !== 'string') {
      if (mode === 'on' && typeof config.provider === 'string') {
        name = config.provider;
      }
      if (mode === 'off' && typeof config.defaultmap === 'string') {
        name = config.defaultmap;
      }
    }

    if (typeof name !== 'undefined' && name === loaded) {
      return;
    }

    loaded = undefined;

    if (typeof name === 'undefined') {
      toast("No map selected. Press <kbd>8</kbd> to select one.");
      return;
    }

    if (mode === 'on') {
      if (!(name in PROVIDERS)) {
        toast("Provider not found. Press <kbd>8</kbd> to select one.");
        return;
      }

      let provider = PROVIDERS[name];

      layer = L.tileLayer(provider.urlTemplate, {
        minZoon: 0,
        maxZoom: 18,
        attribution: provider.attribution,
      });
      bounds = L.latLngBounds(
        L.latLng(-180.0, -180.0),
        L.latLng(180.0, 180.0)
      );

      toast(`<b>ONLINE</b> ${provider.attribution}.`);
      remark();
    } else if (mode === 'off') {
      if (maps.length === 0) {
        toast("No maps found. Press <kbd>7</kbd> to download.");
        return;
      }

      let plasticMap = maps.reduce(
        function(prev, curr) {
          if (curr.name === name || curr.getName() === name) {
            return curr;
          }
          return prev;
        },
        undefined
      );

      if (!plasticMap) {
        toast(`Plastic map '${name}' not found.`);
        return;
      }

      layer = L.tileLayer.plasticMap(plasticMap);
      bounds = undefined;

      layer.on('databaseloaded', function(metadata) {
        bounds = layer.getBounds();
        if (map && bounds) {
          map.flyToBounds(bounds);
          map.once('moveend', function(event) {
            if (map.getBoundsZoom(bounds) < layer.options.minzoom) {
              toast("Too far away. Press <kbd>SL(+)</kbd> to zoom in.");
            }
          });
        }
        toast(`<b>OFFLINE</b> ${metadata.attribution}.`);
        remark();
      });
      layer.on('databaseerror', function(err) {
        let message = "Unknown error";
        if (err) {
          message = err.message || err.error;
        }
        toast(`Database error. ${message}.`);
      });
    }

    if (layer) {
      loaded = name;
      map.addLayer(layer);
    }
  }

  let toast = (function(id) {
    let parent;

    return function(message, type) {
      if (!parent) {
        parent = document.getElementById(id || 'toast');
      }

      let element = document.createElement(
        parent.tagName === 'UL' ? 'LI' : 'DIV'
      );

      if (type) {
        element.className = type;
      }
      element.innerHTML = message;
      parent.appendChild(element);

      setTimeout(function() {
        parent.removeChild(element);
      }, 3000);
    };
  })();

  function error(msg, err) {
    if (err) {
      if (typeof err.name !== 'undefined' && DEBUG) {
        msg = '<b>[' + err.name + ']</b> ' + msg;
      }
      if (typeof err.message !== 'undefined') {
        msg += ' ' + err.message;
      }
      if (typeof err.getMessage !== 'undefined') {
        msg += ' ' + err.getMessage();
      }
      if (typeof err.code !== 'undefined' && DEBUG) {
        msg += ' (' + err.code + ')';
      }
    }
    toast(msg, 'error');
  }

  let compass = (function(id, incOrDec, hiddenTimes, fullIn) {
    let size;
    let width;
    let height;
    let element;
    let scale = 1;
    let hidden = 0;
    let full = fullIn || 150;

    return function() {
      if (map && !width && !height) {
        let mapsize = map.getSize();
        width = mapsize.x;
        height = mapsize.y;
      }
      if (!width || !height) {
        return;
      }
      if (!element) {
        element = document.getElementById(id || 'compass');
        if (!size) {
          size = element.height.baseVal.value;
        }
        element.style.top = ((height - size) / 2).toFixed(0) + 'px';
        element.style.display = hidden > 0 ? 'none' : 'block';
      }
      full--;
      if (hidden > 0) {
        hidden--;
        return;
      }
      if (full === 0) {
        setTimeout(function() { full = fullIn || 150; }, 3000);
      }
      if (full < 0) {
        scale = 1;
        hidden = 0;
      } else {
        const next = scale + (incOrDec ? 0.1 : -0.1);
        if (next <= 0 || next >= (width - 10) / size) {
          scale = 1;
          hidden = hiddenTimes || 17;
        } else {
          scale = next;
        }
      }
      element.style.transform = 'scale(' + scale + ', ' + scale + ')';
      element.style.display = hidden > 0 ? 'none' : 'block';
    };
  })();

  let bottom = (function(id) {
    let element;

    return function(actions, message) {
      if (!element) {
        element = document.getElementById(id || 'bottom');
      }

      element.className = '';
      element.innerHTML = '';

      if (typeof actions === 'undefined') {
        return;
      }
      if (typeof actions !== 'object') {
        throw new Error("Three are bottom actions: LSK, CSK and RSK.");
      }

      let keys;
      let indx = 0;
      let softkeys = element;

      if (Object.keys(actions).length === 3) {
        keys = ['SoftLeft', 'Enter', 'SoftRight'];
      } else if (Object.keys(actions).length === 2) {
        keys = ['SoftLeft', 'SoftRight'];
      } else if (Object.keys(actions).length === 1) {
        keys = ['SoftLeft'];
      } else {
        throw new Error("Bottom actions mismatch.");
      }

      if (message) {
        let paragraph = document.createElement('P');
        paragraph.innerHTML = message;
        element.appendChild(paragraph);
        softkeys = document.createElement('DIV');
        element.appendChild(softkeys);
      }

      softkeys.className = 'softkeys';
      context.bottom = {};

      for (let label in actions) {
        let button = document.createElement('BUTTON');
        button.tabindex = -1; // do not step
        button.innerHTML = label;
        button.onclick = actions[label];
        button.className = keys[indx].toLowerCase();
        context.bottom[keys[indx]] = actions[label];
        softkeys.appendChild(button);
        indx++;
      }
    };
  })();

  let show = (function(id) {
    let screenEl;

    return function(name, html, ctx) {
      if (!screenEl) {
        screenEl = document.getElementById(id || 'screen');
      }

      screenEl.innerHTML = `<dialog${(name ? ' open' : '')}>${(html || "")}</dialog>`;
      screenEl.style.display = name ? 'block' : 'none';

      screen = name || 'map';
      context = ctx ? ctx() : {};
      context.scroll = function(by, elm) {
        if (typeof by === 'number') {
          if (by < 0) {
            screenEl.scrollBy(0, -100);
          } else if (by > 0) {
            screenEl.scrollBy(0, 100);
          }
        }
        if (typeof elm === 'object') {
          screenEl.scrollTo(0, elm.offsetTop - 6);
        }
      };
    };
  })();

  function hide() {
    show(); /* to hide is to show nothing */
  }

  function geodata() {
    if (!geopos) {
      toast("No position information. Press <kbd>1</kbd> to geolocate.");
      return;
    }
    let geoLatLng = L.latLng(geopos);
    let heading = geoextra && geoextra.heading ? geoextra.heading : null;
    let olc = OLC.encode(geoLatLng.lat, geoLatLng.lng);
    let track = '';
    if (feature && feature instanceof L.GPX) {
      track = `
        <div class="track">
          <h5>Track ${feature.get_name()}</h5>
          <small>
            <label>Distance</label>${feature.get_distance().toFixed(0)}m
            <label>Elevation</label>+${feature.get_elevation_gain().toFixed(0)}/-${feature.get_elevation_loss().toFixed(0)}m
            <label>Duration</label>${feature.get_duration_string(feature.get_total_time())}
          </small>
        </div>`;
    }
    show('data', `
      <div class="data">
        <h3>Geographical Position</h3>
        <div id="datacompass">
          <span class="value">` +
            (heading ? (geoextra.speed === 0 ? '-'
              : (geoextra.speed ? geoextra.speed.toFixed(1) + '㎧' : '?'))
              : '?') + `
          </span>
        </div>
        <div class="pos">
          <code>${olc}</code>
          <small>
            <label>lat</label>${geoLatLng.lat}&nbsp;
            <label>lng</label>${geoLatLng.lng}` +
            (geoextra && geoextra.accuracy
              ? `&nbsp;±${geoextra.accuracy.toFixed(0)}m` : '') + `
          </small>` +
          (geoLatLng.alt ? `
            <small>
              <label>alt</label>${geoLatLng.alt}m&nbsp;MSL` +
              (geoextra && geoextra.altitudeAccuracy
                ? `&nbsp;±${geoextra.altitudeAccuracy.toFixed(0)}m` : '') + `
            </small>` : '') + `
        </div>
        ${track}
      </div>`,
      function() {
        let svgencoded = btoa(
          document.getElementById('compass').outerHTML
            .replace(' id="compass"', '')
            .replace(/ style="[^"]+"/, '')
        );
        let datacompass = document.getElementById('datacompass');
        datacompass.style.backgroundImage = `url(data:image/svg+xml;base64,${svgencoded})`;
        if (heading !== null && heading > 0) {
          datacompass.style.transform = `rotate(${heading}deg)`;
        }
        return {};
      }
    );
  }

  function edit(editmarker) {
    if (!(editmarker instanceof L.Marker)) {
      throw new Error("Invalid marker.");
    }
    let olc = OLC.encode(editmarker.getLatLng().lat, editmarker.getLatLng().lng);
    show('marker', `
      <h3>Marker</h3>
      <form id="edit" method="dialog">
        <ul class="choices">
          <li>
            <big>Position</big>
            <code>${olc}</code>
            [${editmarker.getLatLng().lat.toFixed(4)},&nbsp;
            ${editmarker.getLatLng().lng.toFixed(4)}]
          </li>
          <li>
            <big>Symbol</big>` +
            SYMBOLS.map(function(symbol, index) {
              let id = 'sym_' + symbol.toLowerCase();
              let checked = '';
              if (symbol === editmarker.options.alt) {
                checked = ' checked="checked"';
              }
              return `
                <div class="opt">
                  <input${checked} type="radio" id="${id}"
                    name="sym" value="${symbol}">
                  <label for="${id}">
                    <b>${(index + 1)}</b>:&nbsp;
                    <img src="/sym/${symbol}.png" alt="${symbol}" width=16 height=16>
                  </label>
                </div>`;
            }).join('') + `
          </li>
          <li>
            <big>Observation</big>
            <textarea id="obs" name="obs">${editmarker.options.title}</textarea>
          </li>
        </ul>
      </form>`,
      function() {
        let obs = document.getElementById('obs');

        obs.onfocus = function() {
          // Hickhack bottom to use it as "done writing" button.
          document.getElementsByClassName('softkeys')[0].firstElementChild.innerHTML = "Done";
          context.sel = true;
        };
        obs.onblur = function() {
          // Can be "cancel" again.
          document.getElementsByClassName('softkeys')[0].firstElementChild.innerHTML = "Cancel";
          context.sel = false;
        };

        return {
          obs: obs,
          sel: false,

          sym: document.getElementsByName('sym'),

          click: function(key) {
            if (!context.sel) {
              let index = parseInt(key);
              if (typeof index === 'number'
                && typeof SYMBOLS[index - 1] !== 'undefined'
              ) {
                context.sym[index - 1].checked = true;
              }
            }
          },
          prev: function() {
            return /* scroll when */ !context.sel;
          },
          next: function() {
            if (!context.sel) {
              context.obs.focus({preventScroll: false});
            }
            return /* scroll when */ !context.sel;
          },
        };
      });
    bottom({
      "Cancel": function() {
        if (context.sel) {
          context.obs.blur(); // done writing
        } else {
          bottom();
          hide();
        }
      },
      "Save": function() {
        let data = new FormData(document.getElementById('edit'));
        let mark = {pos: editmarker.getLatLng()};

        if (data.has('sym') && data.get('sym')) {
          mark.sym = data.get('sym');
        }
        if (data.has('obs') && data.get('obs')) {
          mark.obs = data.get('obs');
        }

        if (typeof editmarker.markindex === 'number') {
          mapmarks[editmarker.markindex].deleted = true;
        }

        mapmarks.push(mark);

        bottom();
        hide();

        if (marker) {
          marker.closePopup();
          if (map && typeof marker.markindex === 'undefined') {
            marker.removeFrom(map);
          }
          marker = undefined;
        }

        remark(mark);
        movemark(0);
      },
    });
  }

  function choice(title, choices, selected, resolve) {
    show('choice', `
      <h3>${title}</h3>
      <ul class="choices">` +
        choices.map(function(text) {
          if (text === selected) {
            return `<li class="selected">${text}</li>`;
          } else {
            return `<li>${text}</li>`;
          }
        }).join('') + `
      </ul>`,
      function() {
        let lis = document.querySelectorAll('ul.choices li');
        let sel, idx = -1;

        if (lis.length) {
          let index;
          for (index in lis) {
            if (!lis.hasOwnProperty(index)) {
              continue;
            }
            if (lis[index].className === 'selected') {
              sel = lis[index];
              idx = index;
            }
          }
        }

        return {
          choices: lis,
          selected: sel,
          index: Number(idx),
          resolve: resolve,
        };
      }
    );
  }

  function chose(name) {
    if (screen !== 'choice') {
      throw new Error("Cannot chose without choices.");
    }

    let idx = context.index;

    if (typeof name === 'number') {
      if (typeof context.index === 'undefined') {
        idx = 0;
      } else {
        idx = idx + name;

        if (!context.choices.item(idx)) {
          idx = name > 0 ? 0 : context.choices.length - 1;
        }
      }
    } else {
      idx = context.choices.reduce(function(prev, curr, index) {
        if (curr.textContent === name) {
          return index;
        }
        return prev;
      }, -1);
    }

    if (context.selected) {
      context.selected.className = '';
    }

    context.index = idx;
    context.selected = context.choices[idx];
    context.selected.className = 'selected';
    context.scroll(0, context.selected);
  }

  function download() {
    let names = remotes[remotes.length - 1].map(function(download) {
      let size = download.getSize();
      if (size) {
        let humansize = size;
        if (typeof size === 'number') {
          let sizes = ['B', 'KB', 'MB', 'GB'];
          let sizei = Math.floor(Math.log(size) / Math.log(1024));
          let sizen = parseFloat((size / Math.pow(1024, sizei)).toFixed(1));

          humansize = sizen + ' ' + sizes[sizei];
        }
        return download.getName() + ` (${humansize})`;
      }
      return download.getName();
    });
    choice("Chose a remote to download", names, undefined, function(name) {
      let chosen = remotes[remotes.length - 1].find(function(download) {
        return download.getName() === name;
      });
      if (!chosen) {
        return;
      }
      chosen.resolve().then(function(downlist) {
        if (downlist.length === 0) {
          toast(`Download '${name}' not available.`);
        } else if (downlist.length === 1) {
          let down = downlist.pop();

          if (down.isMap()) {
            toast(`Downloading '${name}'... `);

            new Promise(function(resolve, reject) {
              let request = new XMLHttpRequest({mozSystem:true});

              request.onload = function() {
                resolve(request.response);
              };
              request.onerror = function() {
                reject(new Error(
                  `Download of '${down.getUrl()}' failed. ${request.statusText}.`
                ));
              };

              request.open('GET', down.getUrl());
              request.responseType = 'blob';
              request.overrideMimeType('application/octet-stream');
              request.send();
            }).then(function(blob) {
              if (!storage) {
                return Promise.reject(new Error("No device storage found."));
              }
              return new Promise(function(resolve, reject) {
                let creating = storage.addNamed(blob, 'maps/' + down.getName());

                creating.onerror = function () {
                  reject(this.error);
                };
                creating.onsuccess = function() {
                  resolve(blob);
                };
              });
            }).then(function(file) {
              maps.push(filemap(file, down.getBounds()));

              if (file && file.name) {
                toast("Download complete: " + file.name + ".");
              } else {
                toast("Download complete.");
              }
              hide();
            }).catch(function(err) {
              error("Download error.", err);
            });
          } else {
            toast("Download error. Resolved non-map download.");
          }
        } else {
          remotes[remotes.length] = downlist;
          download();
        }
      }).catch(function(err) {
        error("Error resolving download.", err);
      });
      if (remotes.length > 1) {
        remotes.pop();
      }
      return true; /* continue in screen, skip hide */
    });
  }

  function help() {
    if (screen === 'info') {
      hide();
    } else if (screen === 'help') {
      if (DEBUG) {
        let info = `
          <h4>Information</h4>
          <ul class="choices inline">
            <li><big>Mode</big>${mode}</li>
            <li><big>Step</big>${step}</li>
            <li><big>Loaded</big>${loaded}</li>
            <li><big>Maps/Files</big>${maps.length}/${files.length}</li>
            <li>
              <big>Markers<small> (inbounds/all)</small></big>
              ${markers.length}/${mapmarks.length}
            </li>
          </ul>`;
        if (map) {
          info += `
            <h4>Map</h4>
            <ul class="choices inline">
              <li>
                <big>Zoom<small> (now/min/max)</small></big>
                ${map.getZoom()}/${map.getMinZoom()}/${map.getMaxZoom()}
              </li>
              <li>
                <big>Position</big>
                ${map.getCenter().lat.toFixed(4)},&nbsp;
                ${map.getCenter().lng.toFixed(4)}
              </li>
              <li><big>Size</big>${map.getSize().x}x${map.getSize().y}px</li>
            </ul>`;
        } else {
          info += `<big>Map undefined.</big>`;
        }
        show('info', info);
      } else {
        hide();
      }
    } else {
      let help = '';
      switch (screen) {
        case 'map':
          help += `
            <i>Left</i> Close up the view.<br>
            <i>Select</i> Update marker to center.<br>
            <i>Right</i> View larger area.<br>`;
          if (marker) {
            if (typeof marker.markindex === 'undefined') {
              help += `<i>Back</i> Remove last marker.<br>`;
            } else {
              help += `<i>Back</i> Close marker popup.<br>`;
            }
          } else if (circle) {
            help += `<i>Back</i> Remove last know position accuracy circle.<br>`;
          } else {
            help += `<i>Back</i> Quit plastic map.<br>`;
          }
          help += `
            <i>D-Pad</i> Move the map around.<br>
            <i>1</i> Locate current position.<br>
            <i>2</i> Show last known position data.<br>
            <i>Hold-2</i> Display and scale compass.<br>
            <i>4</i> Previous marker.<br>
            <i>Hold-4</i> Previous marker even if outside bounds.<br>`;
          if (marker) {
            if (typeof marker.markindex === 'number') {
              help += `<i>5</i> Update selected marker.<br>`;
            } else {
              help += `<i>5</i> Save marker.<br>`;
            }
          } else {
            help += `<i>5</i> New marker.<br>`;
          }
          help += `
            <i>6</i> Next marker.<br>
            <i>Hold-6</i> Next marker even if outside bounds.<br>
            <i>7</i> Download maps.<br>
            <i>8</i> Change loaded map.<br>
            <i>Hold-8</i> Load feature on top of map.<br>
            <i>9</i> Toggle online/offline mode.<br>`;
          break;
        case 'choice':
          help = `
            <i>Up</i> Select previous.<br>
            <i>Down</i> Select next.<br>
            <i>Select</i> Choose selected.<br>
            <i>Back</i> Cancel selection.<br>`;
          break;
        case 'marker':
          help += `<i>Left</i> Save the marker.<br>`;
          if (context.sel) {
            help += `<i>Back</i> Done writing.<br>`;
          } else {
            help += `<i>Back</i> Cancel marker description.<br>`;
          }
          help += SYMBOLS.map(function(symbol, index) {
            return `<i>${(index + 1)}</i>: Select
              <img src="/sym/${symbol}.png" alt="${symbol}" width=16 height=16>
              <em>(${symbol}.)</em><br>`;
          }).join('');
          break;
        default:
          help += `<i>Back</i> Hide screen.<br>`;
          break;
      }
      help += `<i>0</i> Show this help text.`;
      if (screen === 'map') {
        help += `<hr>
          <p>Offline maps are loaded from <code>/maps/</code> folder.</p>`;
      }
      show('help', `<h4>Help</h4>${help}`);
    }
  }

  function end(html) {
    if (map) {
      map.remove();
    }

    if (html) {
      show('end', html);
    } else {
      show('end', `
        <div class="middle">
          <div>
            <img src="/PlasticMap_112.png" alt="Plastic Map"><br>
            Goodbye
          </div>
        </div>`
      );
    }
  }

  L.Icon.Symbol = L.Icon.extend({
    options: {
      sym: 'wonder',
      iconSize: [25, 25],
      iconAnchor: [12.5, 25],
    },

    initialize: function(options) {
      L.Icon.prototype.initialize.call(this, options);

      this.options.iconUrl = '/sym/' + this.options.sym + '.png';
      this.options.iconRetinaUrl = undefined;
    },
  });

  L.icon.symbol = function(sym, options) {
    if (!options) {
      options = {};
    }
    options.sym = sym;
    return new L.Icon.Symbol(options);
  };

  L.TileLayer.PlasticMap = L.TileLayer.extend({
    options: {
      bounds: undefined,
      crossOrigin: true,
    },

    initialize: function(plasticMap, options) {
      if (plasticMap instanceof String) {
        plasticMap = remotemap(plasticMap);
      } else if (plasticMap instanceof ArrayBuffer) {
        plasticMap = buffermap(plasticMap);
      } else if (plasticMap instanceof File) {
        plasticMap = filemap(plasticMap);
      } else if (plasticMap instanceof Response) {
        plasticMap = filemap(plasticMap);
        plasticMap.name = ':response:';
      }

      this._plasticMap = plasticMap;
      this._databaseIsReady = false;

      plasticMap
        .arrayBuffer()
        .then((buffer) => this.openDatabase(buffer))
        .then(() => {
          this._readMetadata();
          this._databaseIsReady = true;
          this.fire('databaseloaded', this._metadata);
        },
        (err) => {
          this.fire('databaseerror', {error: err, message: err.message});
        });

      L.TileLayer.prototype.initialize.call(this, '', options);
    },

    getBounds: function() {
      if (this.options.bounds instanceof L.LatLngBounds) {
        return this.options.bounds;
      }
      let plasticBounds = this._plasticMap.getBounds();
      if (plasticBounds instanceof L.LatLngBounds) {
        return plasticBounds;
      }
    },

    openDatabase: function(buffer) {
      return initSqlJs({
        locateFile: filename => `/vendor/sql.js/${filename}`
      }).then((SQL) => {
        this._db = new SQL.Database(new Uint8Array(buffer));
        this._stmt = this._db.prepare(
          'SELECT tile_data FROM tiles' +
          ' WHERE zoom_level = :z' +
          ' AND tile_column = :x' +
          ' AND tile_row = :y'
        );
        this.fire('databaseopened', {SQL});
      });
    },

    closeDatabase: function() {
      if (this._db) {
        this._db.close();
        this.fire('databaseclosed', {});
      }
    },

    _readMetadata: function() {
      this._metadata = {};
      this._db.each('SELECT name, value FROM metadata', {}, (row) => {
        this._metadata[row.name] = row.value;
      });

      if ('attribution' in this._metadata) {
        this.options.attribution = this._metadata['attribution'];
      }
      if ('minzoom' in this._metadata) {
        this.options.minzoom = Number(this._metadata['minzoom']);
      }
      if ('maxzoom' in this._metadata) {
        this.options.maxzoom = Number(this._metadata['maxzoom']);
      }
      if ('scheme' in this._metadata) {
        if (this._metadata['scheme'].toString().toLowerCase() === 'tms') {
          this.options.tms = true;
        }
      }
      if ('bounds' in this._metadata) {
        let points = this._metadata['bounds'].split(',');
        if (points.length === 4) {
          if (this.options.tms) {
            this.options.bounds = L.latLngBounds(
              L.latLng(points[1], points[0]),
              L.latLng(points[3], points[2])
            );
          } else {
            this.options.bounds = L.latLngBounds(
              L.latLng(points[0], points[1]),
              L.latLng(points[2], points[3])
            );
          }
        }
      }
      if ('format' in this._metadata) {
        if (this._metadata['format'] === 'jpg') {
          this._format = 'image/jpeg';
        } else if (this._metadata['format'] === 'png') {
          this._format = 'image/png';
        }
      }
    },

    createTile: function(coords, done) {
      if (this._databaseIsReady) {
        return L.TileLayer.prototype.createTile.call(this, coords, done);
      }

      let tile = document.createElement('img');

      if (this.options.crossOrigin || this.options.crossOrigin === '') {
        tile.crossOrigin = this.options.crossOrigin === true ? '' : this.options.crossOrigin;
      }

      tile.alt = '';
      tile.setAttribute('role', 'presentation');

      this.once('databaseloaded', () => {
        L.DomEvent.on(tile, 'load', L.bind(this._tileOnLoad, this, done, tile));
        L.DomEvent.on(tile, 'error', L.bind(this._tileOnError, this, done, tile));

        tile.src = this.getTileUrl(coords);
      });

      return tile;
    },

    getTileUrl: function(coords) {
      if (!this._databaseIsReady) {
        return L.Util.emptyImageUrl;
      }

      let row = this._stmt.getAsObject({
        ':x': coords.x,
        ':y': this._globalTileRange.max.y - coords.y,
        ':z': coords.z
      });

      if ('tile_data' in row) {
        return window.URL.createObjectURL(
          new Blob([row.tile_data] , {
            type: this._format || 'image/png',
          })
        );
      } else {
        return L.Util.emptyImageUrl;
      }
    },
  });

  L.tileLayer.plasticMap = function(plasticMap, options) {
    return new L.TileLayer.PlasticMap(plasticMap, options);
  }

  if (DEBUG) {
    window.addEventListener('error', function(event) {
      bottom();
      end(`
        <h4>Error</h4>${event.message}
        <code>${event.filename}</code> // ${event.lineno}:${event.colno}`);
    });
  }

  document.addEventListener('readystatechange', function(event) {
    if (event.target.readyState === 'complete') {
      init();
    }
  });

  document.addEventListener('keydown', function(event) {
    if (screen === 'end') {
      return;
    }
    if (event.key === 'Backspace') {
      if (!context.sel) {
        event.preventDefault();
      }
      return;
    }

    if (context.bottom) {
      if (typeof context.bottom[event.key] === 'function') {
        let cont = context.bottom[event.key].call(context);
        if (!cont) {
          return;
        }
      }
    }

    if (screen === 'map') {
      switch (event.key) {
        case 'ArrowUp': move(step, 0); break;
        case 'ArrowRight': move(0, step); break;
        case 'ArrowDown': move(step * -1, 0); break;
        case 'ArrowLeft': move(0, step * -1); break;
        case 'SoftLeft': case '+': zoom(1); break;
        case 'SoftRight': case '-': zoom(-1); break;
      }
      if (KEYS.indexOf(event.key) !== -1 && !(event.key in keystate)) {
        keystate[event.key] = {
          ts: Date.now(),
          delay: setTimeout(function() {
            keystate[event.key].interval = setInterval(function() {
              switch (event.key) {
                case '2': compass(); break;
              }
            }, KEYINTERVAL);
          }, KEYDELAY),
          interval: null,
        };
      }
    } else if (screen === 'choice') {
      switch (event.key) {
        case 'ArrowUp': chose(-1); break;
        case 'ArrowDown': chose(1); break;
      }
    } else {
      switch (event.key) {
        case 'ArrowUp':
          let contup = true;
          if (typeof context.prev !== 'undefined') {
            contup = context.prev();
          }
          if (contup) {
            context.scroll(-1);
          }
          break;
        case 'ArrowDown':
          let contdown = true;
          if (typeof context.next !== 'undefined') {
            contdown = context.next();
          }
          if (contdown) {
            context.scroll(1);
          }
          break;
      }
    }
  });

  document.addEventListener('keyup', function(event) {
    if (event.key === '0') {
      bottom();
      help();
    }

    if (event.key === 'Backspace') {
      if (!context.sel) {
        event.preventDefault();
      }
      if (screen === 'map') {
        if (marker) {
          marker.closePopup();
          if (map && typeof marker.markindex === 'undefined') {
            marker.removeFrom(map);
          }
          marker = undefined;
        } else if (circle) {
          if (map) {
            circle.removeFrom(map);
          }
          circle = undefined;
        } else if (changed()) {
          if (localStorage && geopos) {
            localStorage.setItem('geopos', Object.values(geopos).join(','));
          }
          bottom({
            "No": function() {
              bottom();
              end();
              window.close();
            },
            "Save": function() {
              save('plastic-map.json', state()).then(function(file) {
                bottom();
                end();
                window.close();
              }).catch(function(err) {
                bottom();
                error("Cannot save configuration.", err);
              });
            },
          }, "Save changes before leaving?");
        } else {
          if (localStorage && geopos) {
            localStorage.setItem('geopos', Object.values(geopos).join(','));
          }
          end();
          window.close();
        }
      } else if (screen === 'end') {
        let phrases = [
          "See you later.",
          "Hasta la vista, baby!",
          "Bye, bye.",
        ];
        toast(phrases[Math.floor(Math.random() * phrases.length)]);
        window.close();
      } else if (remotes.length > 1) {
        remotes.pop();
        download();
      }  else if(context.sel) {
        /* input box selected, do nothing */
      } else {
        bottom();
        hide();
      }
      return;
    }

    if (event.key === 'Enter' && screen === 'choice') {
      if (context.selected) {
        let cont = context.resolve(
          context.selected.textContent
        );
        if (!cont) {
          hide();
        }
      } else {
        let phrases = [
          "Chose one wisely.",
          "Came on! Only one to chose.",
          "Chose the chosen one.",
          "Don't forget to chose one.",
          "The selected one will be chosen.",
        ];
        toast(phrases[Math.floor(Math.random() * phrases.length)]);
      }
      return;
    }

    if (screen !== 'map') {
      if (typeof context.click !== 'undefined') {
        context.click(event.key);
      }
      return;
    }

    if (KEYS.indexOf(event.key) !== -1 && event.key in keystate) {
      if (keystate[event.key].delay) {
        clearTimeout(keystate[event.key].delay);
      }
      if (keystate[event.key].interval) {
        clearInterval(keystate[event.key].interval);
      }
      const keyts = keystate[event.key].ts;
      delete keystate[event.key];
      if (Date.now() >= keyts + KEYDELAY) {
        switch (event.key) {
          case '4': movemark(-1, true); break;
          case '6': movemark(1, true); break;
          case '8':
            if (files.length === 0) {
              toast("No files found.");
              break;
            }
            let names = files.map(function(file) {
              return file.name.split('/').pop();
            });
            choice("Chose a feature", names, null, function(name) {
              if (typeof name !== 'undefined') {
                if (feature) {
                  if (map) {
                    feature.removeFrom(map);
                  }
                  feature = undefined;
                }

                let file = files.find(function(file) {
                  return file.name.split('/').pop() === name;
                });
                if (file) {
                  let reader = new FileReader();

                  reader.onerror = function(event) {
                    reader.abort();
                    error(`Feature file '${name}' cannot be read.`, event.target.error);
                  };
                  reader.onloadend = function(event) {
                    let data = event.target.result;
                    let ext = name.split('.').pop();
                    if (ext === 'geojson') {
                      try {
                        feature = L.geoJSON(data);
                      } catch (err) {
                        error("GeoJSON loading failed.", err);
                      }
                    } else if (ext === 'gpx') {
                      feature = new L.GPX(data, {
                        async: true,
                        parseElements: ['track', 'route', 'waypoint'],
                        marker_options: {
                          startIconUrl: '/vendor/leaflet-gpx/pin-icon-start.png',
                          endIconUrl: '/vendor/leaflet-gpx/pin-icon-end.png',
                          shadowUrl: '/vendor/leaflet-gpx/pin-shadow.png',
                          wptIconUrls: {
                            '': '/vendor/leaflet-gpx/pin-icon-wpt.png',
                          },
                        },
                      });
                      feature.on('loaded', function(event) {
                        if (map) {
                          event.target.addTo(map);
                          map.fitBounds(event.target.getBounds());
                        }
                      });
                      feature.on('error', function(err) {
                        error("GPX loading failed.", err);
                      });
                    }
                  };
                  reader.readAsText(file);
                } else {
                  toast(`Feature file '${name}' could not be found.`);
                }
              }
            });
            break;
          case 'Enter': delmark(); break;
        }
        return;
      }
    }

    switch (event.key) {
      case '1':
        toast("Seeking Geolocation...");
        locate();
        break;
      case '2': geodata(); break;
      case '4': movemark(-1); break;
      case '5':
        if (!marker) {
          mark();
        }
        edit(marker);
        break;
      case '6': movemark(1); break;
      case '7':
        if (!storage) {
          toast("Download unavailable. No device storage found.");
        } else if (remotes.length === 0) {
          toast("Download unavailable. Configuration missing.");
        } else {
          download();
        }
        break;
      case '8':
        let title;
        let names;
        if (mode === 'on') {
          title = "Chose an online provider";
          names = Object.keys(PROVIDERS);
        } else {
          if (maps.length === 0) {
            toast("No maps found. Press <kbd>7</kbd> to download.");
            break;
          }
          title = "Chose a Plastic Map";
          names = maps.map(function(plasticMap) {
            return plasticMap.name;
          });
        }
        choice(title, names, loaded, function(name) {
          if (typeof name !== 'undefined') {
            reload(name);
          }
        });
        break;
      case '9':
        mode = mode === 'off' ? 'on' : 'off';
        reload();
        break;
      case 'Enter': mark(); break;
    }
  });
})();
