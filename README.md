# Plastic Map for Nokia 8110 4G

![Plastic Map](/app/PlasticMap_112.png)

Non-waterproof offline plastic maps for your KaiOS phone.

## Features

 - Load **offline maps** from mbtiles sources.
 - Download maps from remote server when online.
 - Auto-save last known position as little red dots.
 - Create, update and delete makers within nine categories.
 - Overlay features from GeoJSON or GPX tracks.

## Configuration

Formated as JSON and stored in `/sdcard1/plastic-map.json`.

Example configuration keys and possible values:

```js
{
  "defaultmap": "/sdcard1/maps/world.mbtiles", // offline mbtiles filename
  "provider": "opentopomap.org",               // online tile server
  "center": [0.0, 0.0, 10],                    // start location and zoom
  "mode": "off",                               // starting mode (on/off)
  "markers": [
    {
      "pos": {"lat":0.0, "lng":0.0}, // position as coordinates
      "obs": "Sea",                  // observations, shown when selected
      "sym": "water"                 // symbol used as icon
    }
  ],
  "remotes": [
    // download url to "/sdcard1/maps/" directory
    {
      "url": "http://example.com/pub/guatemala.mbtiles",
      // non-required but recommended (for all remotes types)
      "bounds": [-92.2851, 13.6673, -88.7695, 17.8951],
      "size": 33058816
    },

    // load remote json and show results to download
    {"url": "http://example.com/~jm/backtrip-2022.json", "type": "remotes"}
  ]
}
```

Final configuration file in SD card must not have comments (`//` and after) and should point to existing files so Plastic Map is able to read it, and overwrite it with new markers and settings.

## Symbols

 - **wonder** <img src="app/sym/wonder.png" alt="wonder" height=16 width=16> are for favorite places.
 - **walk** <img src="app/sym/walk.png" alt="walk" height=16 width=16> to mark the way.
 - **view** <img src="app/sym/view.png" alt="view" height=16 width=16> for points worth to see.
 - **sleep** <img src="app/sym/sleep.png" alt="sleep" height=16 width=16> are places to pass the night.
 - **water** <img src="app/sym/water.png" alt="water" height=16 width=16> to drink and fill your bottle.
 - **market** <img src="app/sym/market.png" alt="market" height=16 width=16> to buy provisions.
 - **hitch** <img src="app/sym/hitch.png" alt="hitch" height=16 width=16> where to hitch-hike.
 - **connect** <img src="app/sym/connect.png" alt="connect" height=16 width=16> to access the Internet.
 - **social** <img src="app/sym/social.png" alt="social" height=16 width=16> are places to socialize.


## License

[ISC license](COPYING).

Thanks Banana Hackers and Luxferre (OLC).
