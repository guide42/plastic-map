### Latest Version

### 0.4.0 (2020-11-04)

 - Add red point in the middle of geo location found. Keep them for the session.
 - Compass: More detailled version (auto-generated), scaled down, a show it full after a while.
 - Long-4 and Long-6 will move between markers even if is out of bounds.
 - Delete current selected marker on Long-Enter.
 - Show OLC plus code (Thanks Luxferre) on popup and when editing marker.
 - Replace share with show geographical position information screen.
 - Load GeoJSON and GPX track feature.

### 0.3.0 (2020-10-20)

 - Geo position adds a circle instead of a marker.
 - Create configuration file if doesn't exists.
 - Save last known position to local storage.
 - Show distance and direction from last know position on each marker.
 - Increase compass size when holding "2" key.

### 0.2.0 (2020-08-23)

 - Add toast notifications when goings out of current map bounds.
 - Style changes in choices and keyboard keys.
 - Don't zoom on mark anymore.
 - When creating a marker outside bounds, add it anyway so it doesn't seem that it disappear all the suddenly.

### 0.1.0 (2020-08-11)

First public version.
