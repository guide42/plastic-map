app/vendor:
	mkdir -p app/vendor
	rsync -a --exclude 'leaflet-src.*' --exclude '*.html' node_modules/leaflet/dist/ app/vendor/leaflet/
	rsync -a --exclude 'README.md' --exclude 'LICENSE' --exclude 'package.json' node_modules/leaflet-gpx/ app/vendor/leaflet-gpx/
	rsync -a node_modules/sql.js/dist/sql-*asm.* app/vendor/sql.js/
	rsync -a --exclude 'open-sans-latin-ext-*' node_modules/fontsource-open-sans/files/open-sans-latin-*-normal.woff2 app/vendor/open-sans/
	mkdir -p app/vendor/olc && unzip vendor/Luxferre-OLC-30c5b09.zip -d app/vendor/olc/

application.zip: app/vendor
	(cd app/ && zip -qr - *) > application.zip

plastic-map.zip: application.zip
	zip -q plastic-map.zip application.zip metadata.json
